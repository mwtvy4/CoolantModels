__author__ = 'mwt'
import numpy as np
import math
import matplotlib.pyplot as plt

#define h
h = 2  #basic linear coefficient for now



class transfer:
    def __init__(self,area,mass,specificHeat):
        self.area = area
        self.mass = mass #grams
        self.sH = specificHeat
    def initialize(self,hc,initialTemp):
        self.hc = hc
        self.temp = initialTemp #celcius
        self.joules = (self.temp+273) * self.mass * self.sH
    def update(self,transfer):
        self.joules = self.joules - transfer
        self.temp = (self.joules/(self.mass * self.sH)) - 273
#Coolant system params
clt = transfer(.15,829,4.186)
mat = transfer(.4,100000,1)
rad = transfer(1,912,.9)

clt.initialize(3000,100)
mat.initialize(10,40)
rad.initialize(1,100)

#Create timescale
tstep = .1 #seconds

#Define sim length
simLength = 600 #seconds
timeList = range(0,int(simLength/tstep))

speed = []
for i in timeList:
    #speed.append(3)
    #speed.append(3*math.sin(i*.04))
    speed.append(3*i/len(timeList))
q = list(timeList) # create heat list the same size as the time scale


#iterate and calculate
for i in timeList:
    #q[i] = h * deltaT[i] ## this is the emperical equation for heat in radiator
    joulesIn = 0
    qc = clt.hc * clt.area * (clt.temp - rad.temp) #heat transfer from coolant to radiator
    clt.update(qc - joulesIn)

    mat.hc = 10.45 - speed[i] + (10*(speed[i]**(1.0/2))) #heat transfer from air to radiator
    qm = mat.hc * mat.area * (rad.temp - mat.temp)
    rad.update(qm-qc)

    q[i] = rad.temp


#graph

plt.plot(q)
plt.show()


