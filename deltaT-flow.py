__author__ = 'mwt'
__author__ = 'mwt'
import numpy as np
import math
import matplotlib.pyplot as plt

#define h



class transfer:
    def __init__(self):
        self.deltaT = 0
        self.heatIn = 0

#Coolant system params
clt = transfer()
mat = transfer()
rad = transfer()

#Create timescale
tstep = .1 #seconds

#Define sim length
simLength = 600 #seconds
timeList = range(0,int(simLength/tstep))

speed = []
for i in timeList:
    #speed.append(3)
    #speed.append(3*math.sin(i*.04))
    speed.append(3*i/len(timeList))
q = list(timeList) # create heat list the same size as the time scale


#iterate and calculate
for i in timeList:
    #q[i] = h * deltaT[i] ## this is the emperical equation for heat in radiator
    joulesIn = 0
    qc = clt.hc * clt.area * (clt.temp - rad.temp) #heat transfer from coolant to radiator
    clt.update(qc - joulesIn)

    mat.hc = 10.45 - speed[i] + (10*(speed[i]**(1.0/2))) #heat transfer from air to radiator
    qm = mat.hc * mat.area * (rad.temp - mat.temp)
    rad.update(qm-qc)

    q[i] = rad.temp


#graph

plt.plot(q)
plt.show()


