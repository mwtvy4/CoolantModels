__author__ = 'mwt'
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# create timeList
tstep = .1 #seconds
simLength = 60 #seconds
timeList = range(0,int(simLength/tstep))

#create other inputs
#for i in timeList:
    #power.append(3)
    #power.append(3*math.sin(i*.04))
    #power.append(3*i/len(timeList))

tOUT = list(timeList) # create heat list the same size as the time scale


t1 = 25 #initial temps
t2 = 25
ambient = 25

massFlow = .55 # 4.168 J/gK times 8/60 l/sec
flowFactor = massFlow / tstep

motorLoss = .05
rinehartLoss = .05
radConvection = .09

power = 100.0 # constant... for now

# def update():
#     motorQ = motorLoss * power #motor heat input
#     rinehartQ = rinehartLoss * power #heat input 2
#     t2 = t1 + motorQ/flowFactor + rinehartQ/flowFactor # calculate temp rise
#     t1 = t2 * radConvection #radiator has fixed heat output # calculate temp drop
#     return t1

for i in timeList:
     motorJ = motorLoss * power / tstep #motor energy input
     rinehartJ = rinehartLoss * power / tstep #energy input 2
     ## derive delta t from energy
     t2 = t1 + motorJ/flowFactor + rinehartJ/flowFactor # calculate temp rise
     t1 = t2 - ((t2 - ambient ) * radConvection) #radiator has fixed heat output # calculate temp drop

     tOUT[i] = t1
     print(tOUT[i])

plt.plot(tOUT)
plt.show()